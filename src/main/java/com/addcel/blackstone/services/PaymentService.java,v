head	1.1;
access;
symbols
	QA:1.1.0.2
	Root_QA:1.1;
locks; strict;
comment	@# @;


1.1
date	2018.07.23.19.00.15;	author addcel;	state Exp;
branches;
next	;


desc
@@


1.1
log
@-- Version inicial
@
text
@package com.addcel.blackstone.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.blackstone.bean.Bitacora;
import com.addcel.blackstone.bean.BlackstoneDataConnection;
import com.addcel.blackstone.bean.BlackstoneResponse;
import com.addcel.blackstone.bean.PaymentRequest;
import com.addcel.blackstone.bean.PaymentResponse;
import com.addcel.blackstone.bean.Usuario;
import com.addcel.blackstone.mapper.BitacoraMapper;
import com.addcel.blackstone.mapper.JdbcMapper;
import com.addcel.blackstone.utils.BlackstoneUtils;
import com.google.gson.Gson;

@@Service
public class PaymentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);
	
	public static final String VIEW_START_PAYMENT_FORM = "blackstone/paymentForm";
	
	public static final String VIEW_BLACKSTONE_RESPONSE = "blackstone/blackstoneResponse";
	
	private static Gson GSON = new Gson();
	
	@@Autowired
	private JdbcMapper mapper;
	
	@@Autowired
	private BitacoraMapper bitacoraMapper;
	
	public String autorizacionBlackStone(String json){
		PaymentRequest pago = null;
		Usuario usuario = null;
		BlackstoneResponse response = null;
		BlackstoneDataConnection conn = null;
		try {
			pago = (PaymentRequest) GSON.fromJson(json, PaymentRequest.class);
			LOGGER.info("[BLACKSTONE] - INICIO SOLICITUD PAGO: "+pago.toTrace());
			usuario = mapper.getUsuario(pago.getIdUsuario(), pago.getIdTarjeta());
			if(usuario != null){
				conn = mapper.getBlackstoneData("1");
				insertaBitacora(pago, 1, "es", usuario);
				json  = BlackstoneUtils.blackstoneJsonRequest(conn, pago, usuario);
				LOGGER.info("REQUEST ENVIADO A BLACKSTONE: "+json);
				response = BlackstoneUtils.sendPay2Blackstone(conn.getEndPoint(), json, String.valueOf(pago.getIdTransaccion()));
				actualizaBitacora(pago, response);
				response.setIdBitacora(pago.getIdTransaccion());
			} else {
				response = new BlackstoneResponse();
				response.setResponseCode(-1);
				response.setMessage("Usuario no existente");
			}
			response.setBalance(String.valueOf(pago.getMonto()));
			json = jsonRespuesta(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public String autorizacionBlackStone(long idUsuario, int idApp, String idioma, PaymentRequest pago){
		String json = null;
		Usuario usuario = null;
		BlackstoneResponse response = null;
		BlackstoneDataConnection conn = null;
		try {
			LOGGER.info("[BLACKSTONE] - INICIO SOLICITUD PAGO: "+pago.toTrace());
			usuario = mapper.getUsuario(pago.getIdUsuario(), pago.getIdTarjeta());
			if(usuario != null){
				conn = mapper.getBlackstoneData("1");
				insertaBitacora(pago, idApp, idioma, usuario);
				json  = BlackstoneUtils.blackstoneJsonRequest(conn, pago, usuario);
				LOGGER.info("REQUEST ENVIADO A BLACKSTONE: "+json);
				response = BlackstoneUtils.sendPay2Blackstone(conn.getEndPoint(), json, String.valueOf(pago.getIdTransaccion()));
				bitacoraMapper.insertaBitacoraBlackstone(response);
				actualizaBitacora(pago, response);
				response.setIdBitacora(pago.getIdTransaccion());
			} else {
				response = new BlackstoneResponse();
				response.setResponseCode(-1);
				response.setMessage("Usuario no existente");
			}
			response.setBalance(String.valueOf(pago.getMonto()));
			json = jsonRespuesta(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	private String jsonRespuesta(BlackstoneResponse blackstoneResp){
		PaymentResponse response = new PaymentResponse();
		try {
			if(blackstoneResp.getResponseCode() != 200){
				response.setErrorCode(-1);
			} else {
				response.setAuthorization(blackstoneResp.getAuthorizationNumber());
				response.setReference(blackstoneResp.getServiceReferenceNumber());
				response.setIdBitacora(blackstoneResp.getIdBitacora());
			}
			response.setAmount(Double.valueOf(blackstoneResp.getBalance()));
			response.setMessage(blackstoneResp.getMessage()+ " - "+blackstoneResp.getVerbiage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return GSON.toJson(response);
	}
			

	private void actualizaBitacora(PaymentRequest pago, BlackstoneResponse response) {
		Bitacora bitacora = null;
		int status = 0;
		try {
			bitacora = new Bitacora();
			bitacora.setCodigoError(response.getResponseCode());
			if(response.getResponseCode() != 0) {
				status = -1;
			}
			bitacora.setTicket(response.getMessage());
			bitacora.setIdTransaccion(pago.getIdTransaccion());
//			bitacoraMapper.actualizaBitacora(bitacora);
			bitacoraMapper.actualizaBitacora(response.getResponseCode(), response.getMessage(),
					pago.getIdTransaccion(), response.getAuthorizationNumber(), status);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void insertaBitacora(PaymentRequest pago, int idApp, String idioma, 
			Usuario usuario){
		Bitacora bitacora = null;
		long idTransaccion = 0;
		try {
			bitacora = new Bitacora();
			bitacora.setIdUsuario(pago.getIdUsuario());
			bitacora.setIdProveedor(pago.getIdProveedor());
			bitacora.setIdProducto(pago.getIdProducto());
			bitacora.setConcepto(pago.getConcepto());
			bitacora.setMonto(pago.getMonto());
			bitacora.setImei(pago.getImei());
			bitacora.setDestino("");
			bitacora.setTarjetaCompra(pago.getCardNumber());
			bitacora.setTipo(pago.getTipo());
			bitacora.setSoftware(pago.getSoftware());
			bitacora.setModelo(pago.getModelo());
			bitacora.setWkey(pago.getWkey());
//			bitacoraMapper.insertaBitacora(bitacora);
			bitacoraMapper.insertaBitacora(pago.getIdUsuario(), pago.getIdProveedor(), 
					pago.getIdProducto(), pago.getConcepto(), pago.getMonto(), pago.getImei(),
					pago.getCardNumber(), pago.getTipo(), pago.getSoftware(), pago.getModelo(),
					pago.getWkey(), idTransaccion, idApp, idioma);
			
			pago.setIdTransaccion(idTransaccion);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
}@
