head	1.1;
access;
symbols
	QA:1.1.0.2
	Root_QA:1.1;
locks; strict;
comment	@# @;


1.1
date	2018.07.23.19.00.16;	author addcel;	state Exp;
branches;
next	;


desc
@@


1.1
log
@-- Version inicial
@
text
@package com.addcel.blackstone.services;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.blackstone.bean.Bitacora;
import com.addcel.blackstone.bean.BlackstoneDataConnection;
import com.addcel.blackstone.bean.BlackstoneResponse;
import com.addcel.blackstone.bean.Carriers;
import com.addcel.blackstone.bean.Countries;
import com.addcel.blackstone.bean.DoTopUp;
import com.addcel.blackstone.bean.Product;
import com.addcel.blackstone.bean.TemplateCorreo;
import com.addcel.blackstone.bean.TopUpResponse;
import com.addcel.blackstone.bean.Usuario;
import com.addcel.blackstone.mapper.BitacoraMapper;
import com.addcel.blackstone.mapper.JdbcMapper;
import com.addcel.blackstone.utils.BlackstoneUtils;
import com.addcel.blackstone.utils.Constantes;
import com.addcel.blackstone.utils.SendMail;
import com.addcel.blackstone.ws.client.transactionBroker.BrokerSoapProxy;
import com.addcel.blackstone.ws.client.transactionBroker.Catalog;
import com.addcel.blackstone.ws.client.transactionBroker.PIN;
import com.addcel.blackstone.ws.client.transactionBroker.ProductListItem;
import com.addcel.blackstone.ws.client.transactionBroker.ProductListResponse;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@@Service
public class TopUpService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TopUpService.class);
	
	private BrokerSoapProxy proxy = new BrokerSoapProxy();
	
	@@Autowired
	private JdbcMapper mapper;
	
	@@Autowired
	private BitacoraMapper bitacoraMapper;
	
	private BlackstoneDataConnection conn;
	
	private BlackstoneDataConnection topUpConn;
	
	private static Gson GSON = new Gson();
	
	public Product[] getTopUpByCountry(String countryCode, HttpSession session) {
		Product[]  topups = null;
		try {
			topups = mapper.getTopUpList(countryCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return topups;
	}

	public Carriers[] getCarriers(String countryCode, HttpSession session) {
		Carriers[] carriers = null;
		try {
			LOGGER.info("BLACKSTONE REQUEST - GET CARRIERS - ID SESSION - {} - COUNTRY CODE - {}", session, countryCode);
			carriers = mapper.getCarriersList(countryCode);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("BLACKSTONE REQUEST - GET CARRIERS - ID SESSION - {} - COUNTRY CODE - {}", session, countryCode);
		}
		LOGGER.info("BLACKSTONE REQUEST - GET CARRIERS - ID SESSION - {} - RESPONSE - {}", session, GSON.toJson(carriers));
		return carriers;
	}

	public Product[] getProductsByCarrier(String carrier, HttpSession session) {
		Product[] products = null;
		try {
			LOGGER.info("BLACKSTONE REQUEST - GET PRODUCT BY CARRIER - ID SESSION - {} - COUNTRY CODE - {}", session, carrier);
			products = mapper.getProductsList(carrier);
		} catch (Exception e) {
			LOGGER.error("BLACKSTONE REQUEST - GET PRODUCT BY CARRIER - ID SESSION - {} - COUNTRY CODE - {}", session, carrier);
			e.printStackTrace();
		}
		LOGGER.info("BLACKSTONE REQUEST - GET PRODUCT BY CARRIER - ID SESSION - {} - RESPONSE - {}", session, GSON.toJson(products));
		return products;
	}
	
	public TopUpResponse doTopUp(DoTopUp topup, int idApp, String idioma, HttpSession session) {
		PIN pin = null;
		Usuario usuario = null;
		String json = null;
		BlackstoneResponse responseBlackstone = null;
		long idTransaccion = 0;
		TopUpResponse response = new TopUpResponse();
		try {
			conn = mapper.getBlackstoneData("1");
			topUpConn = mapper.getBlackstoneData("2");
			LOGGER.info("BLACKSTONE REQUEST - GET DO TOP UP - JSON - {}", GSON.toJson(topup));
			usuario = mapper.getUsuario(topup.getIdUsuario(), topup.getIdTarjeta());
			idTransaccion = insertaBitacora(topup, idApp, idioma, usuario); 
			LOGGER.info("ID BITACORA REGISTRADA: {} - USUARIO - {}", idTransaccion, usuario.getNombreUsuario());
			json = BlackstoneUtils.blackstoneJsonRequest(conn, topup, usuario);
			LOGGER.info("REQUEST ENVIADO A BLACKSTONE: "+json);
			responseBlackstone = BlackstoneUtils.sendPay2Blackstone(conn.getEndPoint(), json, String.valueOf(idTransaccion));
			if(Constantes.BLACKSTONE_RESPONSE_CODE_AUTH == responseBlackstone.getResponseCode()) {
				pin = proxy.doTopUp(topUpConn.getMid(), topUpConn.getCid(), 
						topUpConn.getPassword(), "1", topup.getMainCode(), String.valueOf(topup.getAmount()), 
						topup.getPhoneNumber(), 
						topup.getCountryCode(), 1, 1, BlackstoneUtils.TRANSACTION_MODE);
				LOGGER.info("RESPUESTA DE BLACKSTONE: ERROR CODE: "+pin.getErrorCode()
				+", ACCOUNT NUMBER: "+pin.getAccountNumber()+", AUTHORIZATION CODE: "+pin.getAuthorizationCode()
				+", BAR CODE: "+pin.getBarcode()
				+", CURRENCY CODE: "+pin.getCurrencyCode()
				+", ERROR MESSAGE: "+pin.getErrorMessage());
				if(Constantes.BLACKSTONE_RESPONSE_TOPUP.equals(pin.getErrorCode())) {
					LOGGER.info("RECARGA EXITOSA USUARIO: {}", usuario.getNombreUsuario());
					BeanUtils.copyProperties(pin, response);
					response.setErrorCode(pin.getErrorCode());
					response.setErrorMessage("SUCCESS");
					response.setNombre(usuario.getNombre() + " "+ usuario.getApellido());
					response.setConcepto("PAY "+topup.getCarrierName());
					response.setFecha(BlackstoneUtils.getFecha("yyyy-MM-dd HH:mm:ss"));
					response.setReferenceNumber(topup.getPhoneNumber());
					response.setTransactionID(String.valueOf(idTransaccion));
					response.setMaskCard(BlackstoneUtils.maskCard(AddcelCrypto.decryptTarjeta(usuario.getNumTarjeta()), "X"));
					response.setAuthorizationCode(responseBlackstone.getAuthorizationNumber());
					
					response.setBalance(topup.getAmount());
					response.setComision(topup.getComision());
					response.setMaskCard(usuario.getNumTarjeta());
					response.setTotal(topup.getAmount() + topup.getComision());
					
					response.setNombre(usuario.getNombre() + " "+ usuario.getApellido());
					response.setCarrierName(topup.getCarrierName());
					response.setLoginOrMail(usuario.getNombreUsuario());
					TemplateCorreo correo = mapper.getTemplateMail("@@COMPRA_BLACKSTONE", idioma, idApp);
					try {
						SendMail mail = new SendMail();
						mail.generatedMail(response, correo);
					} catch (Exception e) {
						LOGGER.error("FALLO EL ENVIO DE CORREO TOP UP BLACKSTONE: USUARIO - {} - ERROR: {}", usuario.getNombreUsuario(), e.getLocalizedMessage());
						e.printStackTrace();
					}
					
				} else {
					json = BlackstoneUtils.blackstoneJsonRevert(conn, topup, usuario, idTransaccion);
					LOGGER.info("RECARGA NO EXITOSA APLICANDO REVERSO - USUARIO - {}", usuario.getNombreUsuario());
					responseBlackstone = BlackstoneUtils.sendReverse2Blackstone("https://services.bmspay.com/testing/api/Transactions/DoRefund", json, String.valueOf(idTransaccion));
					BeanUtils.copyProperties(responseBlackstone, response);
					response.setErrorCode(String.valueOf(responseBlackstone.getResponseCode()));
					response.setErrorMessage(responseBlackstone.getMessage());
				}
			} else {
				LOGGER.info("COBRO NO EXITOSO USUARIO: {}", usuario.getNombreUsuario());
				BeanUtils.copyProperties(responseBlackstone, response);
				response.setErrorCode(String.valueOf(responseBlackstone.getResponseCode()));
				response.setErrorMessage(responseBlackstone.getMessage());
			}
			actualizaBitacora(topup, responseBlackstone);
		} catch (Exception e) {
			e.printStackTrace();
			response.setErrorCode("-1");
			if("es".equals(idioma)) {
				response.setErrorMessage("Error inesperado.");
			} else {
				response.setErrorMessage("Unexpected error ocurred.");
			}
			
			LOGGER.error("BLACKSTONE REQUEST ERROR - GET DO TOP UP - ID SESSION - {} - ERROR - {}", session.getId(), e.getMessage());
		}
		LOGGER.info("BLACKSTONE RESPONSE - GET DO TOP UP - ID SESSION - {} - JSON - {}", session.getId(), GSON.toJson(response));
		return response;
	}
	
	private void actualizaBitacora(DoTopUp topup, BlackstoneResponse response) {
		int status = 0;
		try {
			if(response.getResponseCode() != 0) {
				status = -1;
			}
			bitacoraMapper.actualizaBitacora(response.getResponseCode(), response.getMessage(),
					topup.getIdTransaccion(), response.getAuthorizationNumber(), status);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private long insertaBitacora(DoTopUp topup, int idApp, String idioma, Usuario usuario) {
		long idTransaccion = 0;
		Bitacora bitacora = null;
		try {
			bitacora = new Bitacora();
			bitacora.setIdUsuario(topup.getIdUsuario());
			bitacora.setIdProveedor(Constantes.BLACKSTONE_PROVEEDOR);
			bitacora.setIdProducto(topup.getId());
			bitacora.setConcepto("PAY "+topup.getCarrierName());
			bitacora.setMonto(topup.getAmount());
			bitacora.setImei(topup.getImei());
			bitacora.setDestino("");
			bitacora.setTarjetaCompra(usuario.getNumTarjeta());
			bitacora.setTipo("");
			bitacora.setSoftware(topup.getSoftware());
			bitacora.setModelo(topup.getModelo());
			bitacora.setWkey(topup.getWkey());
			bitacora.setIdApp(idApp);
			bitacora.setIdioma(idioma);
			bitacoraMapper.insertaBitacora(bitacora);
//			idTransaccion = bitacoraMapper.insertaBitacora(topup.getIdUsuario(), Constantes.BLACKSTONE_PROVEEDOR, 
//					topup.getId(), "PAY "+topup.getCarrierName(), topup.getAmount(), topup.getImei(),
//					usuario.getNumTarjeta(), "", topup.getSoftware(), topup.getModelo(),
//					topup.getWkey(), idTransaccion, idApp, idioma);
			idTransaccion = bitacora.getIdTransaccion();
			topup.setIdTransaccion(bitacora.getIdTransaccion());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idTransaccion;
	}

	public ProductListResponse getProductList(HttpSession session) {
		ProductListResponse productList = null;
		try {
			conn = mapper.getBlackstoneData("2");
			productList = proxy.getProductList("92867", "10074872","1nKBFE06", "1", 1);
			mapper.deleteProducts();
			for(ProductListItem item: productList.getProducts()){
				LOGGER.info("CARRIER NAME: "+ item.getCarrierName()+
						", NAME: "+item.getName()+", CODE: "+item.getCode()+", TYPE: "+item.getType()+
						", GROUPER ID: "+item.getGrouperId()+", INSTRUCTIONS: "+item. getInstructions());
				mapper.insertProducts(item);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("BLACKSTONE REQUEST ERROR - PRODUCTS PROFILE - ID SESSION - {} - ERROR - {}", session, e.getMessage());
		}
		return productList;
	}
	
	public Catalog[] productsProfile(HttpSession session) {
		Catalog catalog[] = null;
		ProductListResponse response = null;
		try {
			conn = mapper.getBlackstoneData("2");
			LOGGER.info("BLACKSTONE REQUEST - PRODUCTS PROFILE - ID SESSION - {}", session);
//			catalog = proxy.getProductProfiles(conn.getMid(), conn.getCid(), 
//					conn.getPassword(), "1", BlackstoneUtils.TRANSACTION_MODE);
			response = proxy.getProductList("92867", "10074872","1nKBFE06", "1", 1);
			LOGGER.info("BLACKSTONE RESPONSE - PRODUCTS PROFILE - ID SESSION - {}", session, response.toString());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("BLACKSTONE REQUEST ERROR - PRODUCTS PROFILE - ID SESSION - {} - ERROR - {}", session, e.getMessage());
		}
		return catalog;
	}

	public Catalog productsCatalog(Integer profileId, HttpSession session) {
		Catalog catalog = null;
		try {
			conn = mapper.getBlackstoneData("2");
			LOGGER.info("BLACKSTONE REQUEST - PRODUCTS CATALOG - ID SESSION - {} - PROFILE ID - {}", session, profileId);
			catalog = proxy.getProductsCatalog(conn.getMid(), conn.getCid(), 
					conn.getPassword(), "", profileId, BlackstoneUtils.TRANSACTION_MODE);
			LOGGER.info("BLACKSTONE RESPONSE - PRODUCTS CATALOG - ID SESSION - {} - JSON - {}", session, catalog.toString());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("BLACKSTONE REQUEST ERROR - PRODUCTS CATALOG - ID SESSION - {} - ERROR - {}", session, e.getMessage());
		}
		return catalog;
	}

	/*public Destination[] getAllDestinations(HttpSession session) {
		Destination[] destinations = null;
		try {
			conn = mapper.getBlackstoneData("2");
			LOGGER.info("BLACKSTONE REQUEST - GET ALL DESTINATIONS - ID SESSION - {}", session);
			destinations = proxy.getAllDestinations(conn.getMid(), conn.getCid(), 
					conn.getPassword(), "", BlackstoneUtils.TRANSACTION_MODE);
			LOGGER.info("BLACKSTONE RESPONSE - GET ALL DESTINATIONS - ID SESSION - {}", session, destinations.toString());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("BLACKSTONE REQUEST ERROR - GET ALL DESTINATIONS - ID SESSION - {} - ERROR - {}", session, e.getMessage());
		}
		return destinations;
	}*/

	public Countries[] getCountries(HttpSession session) {
		Countries[] contries = null;
		try {
			contries = mapper.getContriesList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contries;
	}

}@
